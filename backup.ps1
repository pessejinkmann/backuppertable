﻿# ######################################## #
# Title: BackupPerTable                    #
# Author: AGAG                             #
#                                          #
# Description: Qualquer necessidade de     #
# alteração avisar o autor deste script.   #
# ######################################## #

Add-Type -AssemblyName System.Windows.Forms

# DEFINERS & PATHS
$config = $PSScriptRoot + '\config.ini'

$default           = $PSScriptRoot + '\util\default.ini'
$aes               = $PSScriptRoot + '\util\aes.key'
$passwordTxt       = $PSScriptRoot + '\util\password.txt'
$mysqlConnectorDLL = $PSScriptRoot + '\util\MySQL.Data.dll'

$log   = $PSScriptRoot + "\logs\cpj_$(Get-date -format 'hh_mm_ss_-_dd_MM_yyyy').log"
$tableList = $PSScriptRoot + '\logs\tables_file.list'

$zipName = "cpj_$(Get-date -format 'hh_mm_ss_-_dd_MM_yyyy').zip"
$zipPath = $PSScriptRoot + "\backups\cpj_$(Get-date -format 'hh_mm_ss_-_dd_MM_yyyy').zip"

# INI Extractor
if([System.IO.File]::Exists($config)) {Get-Content $config | foreach-object -begin {$h=@{}} -process { $k = [regex]::split($_,'='); if(($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) { $h.Add($k[0], $k[1]) } } }
	
# [AUDITORIA] 
$global:audPessoa	= $h.auduser

# [BANCO] 
$global:dbServidor	= $h.bkpservidor
$global:dbUsuario	= $h.bkpusuario
$global:dbSenha	= $h.bkpsenha
$global:dbPorta	= $h.bkpporta
$global:dbDatabase	= $h.bkpbase
$global:dbEngine	= $PSScriptRoot + '\util\' + $h.bkpmysql

# [MAIL] 
$global:mailAtivar	= $h.mailativar
$global:mailAsst 	= $h.mailassunto
$global:mailMsg	= $h.mailmensagem
$global:mailTo		= $h.mailpara

# [SMTP] 
$global:smtpUser	= $h.smtpuser
$global:smtpPsswd	= $h.smtppssw
$global:smtpServer	= $h.smtpserver
$global:smtpPorta	= $h.smtpport


function RunMySQLQuery 
{
    Param(
        [Parameter(
            Mandatory = $true,
            ParameterSetName = '',
            ValueFromPipeline = $true)]
        [string]$query,
        [Parameter(
            Mandatory = $true,
            ParameterSetName = '',
            ValueFromPipeline = $true)]
        [string]$connectionString
    )

    Begin {}

    Process {
        try {
            [void][system.reflection.Assembly]::LoadFrom($mysqlConnectorDLL)
            $connection = New-Object MySql.Data.MySqlClient.MySqlConnection
            $connection.ConnectionString = $ConnectionString
            $connection.Open()
            $command = New-Object MySql.Data.MySqlClient.MySqlCommand($query, $connection)
            $dataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($command)
            $dataSet = New-Object System.Data.DataSet
            $recordCount = $dataAdapter.Fill($dataSet, "data")
            & {foreach ($row in $DataSet.Tables["data"].Rows) { 
                $row[0].ToString().Trim()
            } } | set-content $tableList
        } catch {
			WriteLog("$connectionString | $query | $Error[0]");
			if ($Error[0] -like '*Authentication with old password no longer supported*' ) { $msgBoxInput3 =  [System.Windows.Forms.MessageBox]::Show('ATENÇÃO: Antes de continuar criar um usuário no banco de dados sem a opção old_password e alterar arquivo INI.','Primeira Execução','Ok','ERROR') }
            stop-process -Id $PID
        } Finally {
            $connection.Close()
        }
    }
	
    End { }
}

function InsertValueINI
{
	Param ([string]$parametro,[string]$valor)
	$parametroafter=$parametro + "=" 
	$parametrobefore=$parametro + "=" + $valor
	(Get-Content $config) -replace $parametroafter, $parametrobefore | Set-Content $config
}

function WriteLog
{
    Param ([string]$textLog)
	$textLogDate = '[' + $(Get-date -format 'hh:mm:ss dd/MM/yyyy') + '] ' + $textLog
	if(![System.IO.File]::Exists($log)) { New-Item -ItemType "file" -Path $log }
	Add-Content $log $textLogDate
	Write-Host $textLogDate
}

function VerificaConfig 
{
    if(![System.IO.File]::Exists($config))
    {
	    WriteLog("Necessita configurar arquivo de configuracao ...");
        $msgBoxInput3 =  [System.Windows.Forms.MessageBox]::Show('ATENÇÃO: Configuração necessária, leia o README.md!!','Nova Instalação Detectada','Ok','Information')

        WriteLog("Copiando arquivo padrao ...");
	    Copy-Item $default -Destination $config -Recurse

	    try {
			$resultBackup = Invoke-Expression (Show-Command BackupConfiguration -PassThru)
		} catch { 
			WriteLog("Erro, finalizado pelo usuário.")
			Remove-Item $config
			stop-process -Id $PID
		}

        InsertValueINI -parametro "auduser" -valor $resultBackup.Auditoria
        InsertValueINI -parametro "bkpservidor" -valor $resultBackup.Servidor
        InsertValueINI -parametro "bkpusuario" -valor $resultBackup.User
        InsertValueINI -parametro "bkpsenha" -valor $resultBackup.Password
        InsertValueINI -parametro "bkpbase" -valor $resultBackup.Database
        InsertValueINI -parametro "bkpporta" -valor $resultBackup.Porta
        InsertValueINI -parametro "bkpmysql" -valor $resultBackup.MySQL

        VerificaLogin;
    } else { 
		WriteLog("Config.ini OK ..."); 
	}
}

function VerificaLogin 
{
    $msgBoxInput =  [System.Windows.Forms.MessageBox]::Show('Deseja ativar envio por e-mail?','Ativar E-mail?','YesNo','Question')

    switch  ($msgBoxInput) {
        'Yes' 
        { 
			try {
				$resultSMTP = Invoke-Expression (Show-Command SMTPConfiguration -PassThru)
			} catch { 
				WriteLog($_.Exception.Message)
				Remove-Item $config
				stop-process -Id $PID
			}

            InsertValueINI -parametro "mailativar" -valor "sim"
            InsertValueINI -parametro "mailassunto" -valor $resultSMTP.Assunto
            InsertValueINI -parametro "mailmensagem" -valor $resultSMTP.Mensagem
            InsertValueINI -parametro "mailpara" -valor $resultSMTP.Destinatario
            InsertValueINI -parametro "smtpserver" -valor $resultSMTP.SMTPServer
            InsertValueINI -parametro "smtpport" -valor $resultSMTP.SMTPPorta
            WriteLog("E-mail configurado...");
            EncriptPassword;
        } 'No'  { 
            WriteLog("E-mail não configurado...");
            InsertValueINI -parametro "mailativar" -valor "não" 
        }
    }

    $msgBoxInput2 = [System.Windows.Forms.MessageBox]::Show('Deseja realizar um backup agora?','Fazer Backup?','YesNo','Question')
    switch  ($msgBoxInput2) {
        'Yes' { 
			WriteLog("Finalizando configuração...");
			Get-Content $config | foreach-object -begin {$h=@{}} -process { $k = [regex]::split($_,'='); if(($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) { $h.Add($k[0], $k[1]) } }
			$global:audPessoa	= $h.auduser
			$global:dbServidor	= $h.bkpservidor
			$global:dbUsuario	= $h.bkpusuario
			$global:dbSenha	= $h.bkpsenha
			$global:dbPorta	= $h.bkpporta
			$global:dbDatabase	= $h.bkpbase
			$global:dbEngine	= $PSScriptRoot + '\util\' + $h.bkpmysql
			$global:mailAtivar	= $h.mailativar
			$global:mailAsst 	= $h.mailassunto
			$global:mailMsg	= $h.mailmensagem
			$global:mailTo		= $h.mailpara
			$global:smtpUser	= $h.smtpuser
			$global:smtpPsswd	= $h.smtppssw
			$global:smtpServer	= $h.smtpserver
			$global:smtpPorta	= $h.smtpport
		}
        'No'  { stop-process -Id $PID }
    }
}

function EncriptPassword
{
    $Cred = (Get-Credential);

    $global:smtpUser = $Cred.UserName
    $global:smtpUser = "smtpuser=" + $global:smtpUser
    (Get-Content $config) -replace "smtpuser=", $global:smtpUser | Set-Content $config 
       
    $Cred.Password | ConvertFrom-SecureString -key (get-content $aes) | set-content $passwordTxt
    $smtppssw = Get-Content $passwordTxt
    $temp = "smtppssw=" + $smtppssw
    (Get-Content $config) -replace "smtppssw=", $temp | Set-Content $config
}

function EnviarResultado
{
    Param ([string]$Attachments)
    if($global:mailAtivar.equals('sim')) {
        WriteLog("Enviando e-mail ...");

        $SecurePassword = Get-Content $passwordTxt | ConvertTo-SecureString -Key (Get-Content $aes)
        $temp = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
        $UnsecurePassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($temp)

		try { 
			$Message = New-Object System.Net.Mail.MailMessage
			$Message.subject=$global:mailAsst
			$Message.body=$global:mailMsg
			$Message.to.add($global:mailTo)
			$Message.from=$global:smtpUser
			$Message.attachments.add($Attachments)

			$SMTPClient = New-Object Net.Mail.SmtpClient($global:smtpServer, $global:smtpPorta) 
			$SMTPClient.EnableSsl = $true
			$SMTPClient.Credentials = New-Object System.Net.NetworkCredential($global:smtpUser, $UnsecurePassword);
			$SMTPClient.send($Message)
		} catch { 
			WriteLog("Erro no envio de e-mail."); 
		} 
		finally { }
    }
}

function CompactarBackup
{
	$exePath = $PSScriptRoot + '\util\7za.exe'
	$sqlPath = $PSScriptRoot + '\tables\*.sql'

	WriteLog("Compactando arquivos SQLs ...")
	cmd /c $exePath a -tzip -mmt $zipPath $sqlPath
	WriteLog("Arquivos compactados com sucesso em $zipPath ")
}

function FileSizeCalc
{
    Param ([int64]$size)
    if ($size -gt 1TB)     {[string]::Format("{0:0.0000} TB", $size / 1TB)}
    elseif ($size -gt 1GB) {[string]::Format("{0:0.0000} GB", $size / 1GB)}
    elseif ($size -gt 1MB) {[string]::Format("{0:0.0000} MB", $size / 1MB)}
    elseif ($size -gt 1KB) {[string]::Format("{0:0.0000} KB", $size / 1KB)}
    elseif ($size -gt 0)   {[string]::Format("{0:0.0000} B", $size)}
    else {""}
}

function GravaAuditoria
{
	WriteLog("Gravando log na auditoria ...");
	$fileSize = FileSizeCalc((Get-Item $zipPath).length);
	$zipPath = $zipPath.Replace('\', '\\')
	
    $connString = "server=" + $global:dbServidor + ";port=" + $global:dbPorta + ";uid=" + $global:dbUsuario + ";pwd=" + $global:dbSenha + ";database=" + $global:dbDatabase
	$audQuery = "insert into auditoria (usuario, aviso_usuario, data_hora, operacao, tabela, chave, ocorrencia) values (9999, $global:audPessoa, now(0), 'I', 'BKP', '$zipName', concat('Backup realizado na pasta $zipPath com tamanho de $fileSize.'))"
	WriteLog("Query: $audQuery");
    RunMySQLQuery -query $audQuery -connectionString $connString;
}

function BackupConfiguration 
{
    param (
        [string]$Auditoria,
        [string]$Servidor,
        [string]$Username,
        [string]$UserPasword,
        [string]$Database,
        [string]$Porta,
        [validateset("mysqldump.exe", "mysqldump_64.exe","mysqldump_mariadb.exe")]
        [string]$MySQL
    )
    [pscustomobject]@{
        Auditoria = $Auditoria
        Servidor = $Servidor
        User = $Username
        Password = $UserPasword
        Database = $Database
        Porta = $Porta
        MySQL = $MySQL
    }
}

function SMTPConfiguration 
{
    param (
        [string]$Assunto,
        [string]$Mensagem,
        [string]$Destinatario,
        [string]$smtpServer,
        [string]$smtpPorta
    )
    [pscustomobject]@{
        Assunto = $Assunto
        Mensagem = $Mensagem
        Destinatario = $Destinatario
        SMTPServer = $smtpServer
        SMTPPorta = $smtpPorta
    }
}

function MainBackup
{
    VerificaConfig;
    WriteLog("Iniciando Bakcup ...");
	WriteLog("Lendo tabelas do sistema ...");
     
    $connString = "server=" + $global:dbServidor + ";port=" + $global:dbPorta + ";uid=" + $global:dbUsuario + ";pwd=" + $global:dbSenha + ";database=" + $global:dbDatabase
    $bkpQuery = "show tables"
    WriteLog($bkpQuery);
    RunMySQLQuery -query $bkpQuery -connectionString $connString;
    
    if([System.IO.File]::Exists($tableList)) {
        foreach($line in [System.IO.File]::ReadLines('.\logs\tables_file.list'))
        {
            if (!$line.Contains("Tables")) {
                $tableName = $PSScriptRoot + '\tables\' + $line + '.sql'
                WriteLog("Realizando backup da tabela $line em $tableName");
                cmd /c $global:dbEngine --user=$global:dbUsuario --password=$global:dbSenha --host=$global:dbServidor --port=$global:dbPorta  --skip-opt --quick --single-transaction --disable-keys --default-character-set=latin1 --set-charset --extended-insert --create-options $global:dbDatabase $line --result-file=$tableName --log-error=$log
            }
        }
    } else { 
		WriteLog("Verifique, não foi possivel ler as tabelas do banco.")
        stop-process -Id $PID
	}
	
	$tableName = $PSScriptRoot + '\tables\' + 'all_functions_' + $global:dbDatabase + '.sql'
	WriteLog("Realizando backup das functions em $tableName");
	cmd /c $global:dbEngine --user=$global:dbUsuario --password=$global:dbSenha --host=$global:dbServidor --port=$global:dbPorta --skip-opt --quick --routines --skip-triggers --no-create-info --no-data --no-create-db $global:dbDatabase --result-file=$tableName --log-error=$log
	
	CompactarBackup;
	WriteLog("Backup finalizado com sucesso.")
	GravaAuditoria;
	WriteLog("Gravando log na auditoria.")
	EnviarResultado($log);
}

MainBackup