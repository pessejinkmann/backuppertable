![MySQL Logo](http://s.glbimg.com/po/tt/f/original/2012/04/17/mysql-logos.gif)

BackupMySQL per Table
=================================

Introdução
------------------
Script de backup de bases MySQL em PowerShell. Desenvolvida para utilização na rotina de operação das bases dos clientes da Preâmbulo Tecnologia LTDA.

Instruções
------------------
1. Script deve ser executado em um Path que não contenha espaços.

2. Primeiramente, criar novo usuário no banco de dados para o script rodar.
2a. Usuário deve ser criado sem a opção old_password.

3. Para configurar e-mail, por gentileza, inserir informações SMTP do cliente.

Configurações
------------------
* Inserir usuário que deverá receber aviso no CPJ-3C
* Criar usuário no banco de dados SEM set old_password
* Para ativar/desativar envio por email inserir na tag ativar (sim/não)
* Para configurar o e-mail pela primeira vez, deixar as taga smtpuser e smtppssw vazias
* Opções de Dump - mysqldump.exe | mysqldump_.exe | mysqldump_mariadb.exe

Config.ini
------------------

```
[Auditoria]
user=1

[Backup]
servidor=localhost
usuario=backup
senha=backup
base=alexandre
porta=3306
; Opções - mysqldump.exe | mysqldump_.exe | mysqldump_mariadb.exe
mysql=mysqldump_mariadb.exe

[Email] 
ativar=não
assunto=Backup CPJ
mensagem=Backup realizado no cliente _____________________, segue arquivo de log em anexo. Atenciosamente,
to=

[SMTP]
smtpuser=
smtppssw=
smtp=smtp.office365.com
port=587
```

Licença
------------------
Preâmbulo Tecnologia - [Nosso site](https://www.preambulo.com.br/)

Alexandre Gouveia - alexandre.goncalves@preambulo.com.br

Copyright 2019